# Tiny Editor in C

This git project has been created in order to learn and practice C language. 
the aim is to build a tiny editor, inspired by this 
[tutorial](http://viewsourcecode.org/snaptoken/kilo/)

## Agenda

* Week 15 : Chap 1 - Setup
* Week 16 : Chap 2 - Entering raw mode
* Week 17 : Chap 3 - Raw input and output
* Week 18 : Chap 4 - A text viewer
* Week 19 : Chap 5 - A text editor
* Week 20 : Chap 6 - Search
* Week 21 : Chap 7 - Syntax highlighting
* Week 22 : Chap 8 - Appendices
